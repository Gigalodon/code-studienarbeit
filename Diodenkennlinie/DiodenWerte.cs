﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diodenkennlinie
{
    class DiodenWerte : IComparable
    {
        private double voltage_value = 0;
        private double ampere_value = 0;

        public DiodenWerte(string input)
        {
            string[] s = input.Split(';');

            try
            {
                voltage_value = Convert.ToDouble(s[0].Replace('.', ','));
                ampere_value = Convert.ToDouble(s[1].Replace('.', ','));
            }
            catch
            {
                throw new Exception();
            }
            
        }

        public int CompareTo(object x)
        {
            if (x == null) return 1;

            DiodenWerte lineA;
            try
            {
                lineA = (DiodenWerte)x;
            }
            catch
            {
                return 1;
            }
          

            if (voltage_value < lineA.getVoltage)
            {
                return -1;
            }
            if (voltage_value == lineA.getVoltage)
            {
                return 0;
            }
            return 1;
        }

        public double getVoltage
        {
            get
            {
                return voltage_value;
            }
        }
        public double getAmpere
        {
            get
            {
                return ampere_value;
            }
        }
    }
}

