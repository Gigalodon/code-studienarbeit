﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Helpers;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using System.ComponentModel;
using System.Timers;
using System.Windows.Threading;
using System.Diagnostics;
using LiveCharts.Definitions.Charts;
using LiveCharts.Charts;
using Microsoft.Win32;
using System.IO;

namespace Diodenkennlinie
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        SerialPort serialPort;
        bool enableStartButton;
        int selectedIndex;
        List<string> ports;
        List<DiodenWerte> ListDiodenWerte = new List<DiodenWerte>();
        List<double> volt = new List<double>();
        DispatcherTimer timer = new DispatcherTimer();
        DispatcherTimer searchPorts = new DispatcherTimer();
        Stopwatch sw;
        string status;
        bool isWorkflowInProgress;
        bool isWorkflowFinished;
        bool finished;
        Series l = null;
        Axis X;
        Axis Y;
        List<SerialPort> SP = new List<SerialPort>();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            this.InitializeChart();
            this.Status = "Bitte vebinden Sie sich mit dem Gerät.";
            this.IsWorkflowInProgress = false;
            this.IsWorkflowFinished = false;
            this.SelectedIndex = 0;
            this.DetectInterfaces();
        }

        public List<string> Ports
        {
            get
            {
                return this.ports;
            }
            set
            {
                this.ports = value;
                this.OnPropertyChanged(nameof(Ports));
            }
        }

        public int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
            set
            {
                this.selectedIndex = value;
            }
        }

        public string SelectedPort
        {
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    this.serialPort = new SerialPort();
                    this.serialPort.BaudRate = 9600;
                    this.serialPort.PortName = value;

                    try
                    {
                        this.serialPort.Open();
                    }
                    catch
                    {

                    }
                    SP.Add(serialPort);
                    this.Status = "Bereit";
                }
            }
        }

        public bool EnableStartButton
        {
            get
            {
                return enableStartButton;
            }
            set
            {
                this.enableStartButton = value;
                this.OnPropertyChanged(nameof(this.EnableStartButton));
            }
        }

        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
                this.OnPropertyChanged(nameof(Status));
            }
        }

        public bool IsWorkflowInProgress
        {
            get
            {
                return isWorkflowInProgress;
            }
            set
            {
                isWorkflowInProgress = value;
                this.OnPropertyChanged(nameof(this.IsWorkflowInProgress));
            }
        }

        public bool IsWorkflowFinished
        {
            get
            {
                return isWorkflowFinished;
            }
            set
            {
                isWorkflowFinished = value;
                this.OnPropertyChanged(nameof(this.IsWorkflowFinished));
            }
        }

        public void InitializeChart()
        {
            AxesCollection acX = new AxesCollection();
            AxesCollection acY = new AxesCollection();
            X = new Axis();
            X.MaxValue = 10;
            X.MinValue = -10;
            X.Title = "V";
            X.Separator.StrokeThickness = 5;
            X.Separator.IsEnabled = true;
            Y = new Axis();
            Y.MaxValue = 35;
            Y.MinValue = -35;
            Y.Title = "mA";
            X.FontSize = 20;
            Y.FontSize = 20;
            acX.Add(X);
            acY.Add(Y);

            AxisSection axS = new AxisSection();
            axS.Value = 0;
            axS.StrokeThickness = 3;

            AxisSection ayS = new AxisSection();
            ayS.Value = 0;
            ayS.StrokeThickness = 3;

            SectionsCollection co = new SectionsCollection();
            co.Add(ayS);
            Y.Sections = co;

            SectionsCollection col = new SectionsCollection();
            col.Add(axS);
            X.Sections = col;
            
            chart.AxisX = acX;
            chart.AxisY = acY;
        }

        /// <summary>
        /// Fügt eine neue Diodenlinie zur Chart dazu
        /// </summary>
        public void AddLine()
        {
            chart.Series.Remove(l);
            l = new LineSeries();
            l.Stroke = Brushes.Blue;
            l.Fill = Brushes.Transparent;
            ChartValues<ObservablePoint> val = new ChartValues<ObservablePoint>();

            foreach (DiodenWerte wert in ListDiodenWerte)
            {
                val.Add(new ObservablePoint(wert.getVoltage, wert.getAmpere));
            }
            l.Values = val;

            try
            {
                double maxAmp = ListDiodenWerte.Max(w => w.getAmpere);
                double minAmp = ListDiodenWerte.Min(w => w.getAmpere);
                double maxVolt = ListDiodenWerte.Max(w => w.getVoltage);
                double minVolt = ListDiodenWerte.Min(w => w.getVoltage);

                X.MinValue = minVolt;
                X.MaxValue = maxVolt;
                Y.MinValue = minAmp - 1;
                Y.MaxValue = maxAmp + 1;
            }
            catch
            {
                this.Status = "Es wurden keine Werte übermittelt";
            }

            chart.Series.Add(l);
        }

        /// <summary>
        /// Initialisiert ein Event, dass die angeschlossenen Ports überprüft
        /// </summary>
        public void DetectInterfaces()
        {
            this.Ports = SerialPort.GetPortNames().ToList();

            if (this.Ports.Count > 0)
            {
                this.EnableStartButton = true;
            }

            searchPorts.Interval = TimeSpan.FromSeconds(2);
            searchPorts.Tick -= this.OnSearchPortEvent;
            searchPorts.Tick += this.OnSearchPortEvent;
            searchPorts.Start();
        }

        /// <summary>
        /// Überprüft ob neue Schnittstellen angeschlossen wurden, bzw. alte entfernt wurden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSearchPortEvent(object sender, EventArgs e)
        {
            List<string> NewPorts = SerialPort.GetPortNames().ToList();

            foreach(SerialPort s in SP)
            {
                if(s.IsOpen)
                {
                    continue;
                }
                else
                {
                    NewPorts.Remove(s.PortName);
                }

            }

            if (NewPorts != this.Ports)
            {
                this.Ports = NewPorts;
            }

            if (this.Ports.Count != 0)
            {
                this.EnableStartButton = true;
            }
            else
            {
                this.EnableStartButton = false;
            }
        }

        /// <summary>
        /// Startet den Messvorgang
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {

            if (this.serialPort != null && this.serialPort.IsOpen)
            {
                this.EnableStartButton = false;
                this.IsWorkflowInProgress = true;
                this.IsWorkflowFinished = false;
                this.finished = false;
                this.ListDiodenWerte = new List<DiodenWerte>();
                this.Status = "Führe Messung durch...";

                timer.Interval = TimeSpan.FromSeconds(1);
                timer.Tick -= this.OnTimedEvent;
                timer.Tick += this.OnTimedEvent;
                timer.Start();

                sw = Stopwatch.StartNew();

                this.serialPort.Write("Start\n");
                this.serialPort.DataReceived -= new SerialDataReceivedEventHandler(this.OnDataRecieved);
                this.serialPort.DataReceived += new SerialDataReceivedEventHandler(this.OnDataRecieved);
            }
            else if (this.serialPort.IsOpen == false)
            {

                this.Status = "Mit der Verbindung ist ein Fehler aufgetreten. Wenn dieser Fehler öfters erscheint, starten Sie das Programm erneut.";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Überprüft die Messung auf Fehler und beendet die Messung
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnTimedEvent(object sender, EventArgs e)
        {
            timer.Stop();
            this.serialPort.DataReceived -= new SerialDataReceivedEventHandler(this.OnDataRecieved);
            this.ListDiodenWerte.Sort();
            this.AddLine();
            this.EnableStartButton = true;
            this.IsWorkflowInProgress = false;
            this.IsWorkflowFinished = true;

            if (finished == false)
            {
                this.Status = "Bei der Datenübertragung ist ein Fehler aufgetreten. Bitte überprüfen Sie ob alle Schnittstellen richtig angeschlossen sind und probieren Sie es erneut.";
            }
            else
            {
                this.Status = "Die Messung war erfolgreich.";
            }
        }

        /// <summary>
        /// Verarbeitet empfangene Messwerte
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnDataRecieved(object sender, SerialDataReceivedEventArgs e)
        {
            timer.Stop();
            timer.Interval = new TimeSpan(0, 0, 0, 1);
            timer.Start();

            TimeSpan ts = sw.Elapsed;
            if (ts.TotalSeconds > 120)
            {
                this.serialPort.DataReceived -= new SerialDataReceivedEventHandler(this.OnDataRecieved);
            }

            string input = serialPort.ReadLine();

            switch (input.Trim(' ', '\n', '\r'))
            {
                case "DONE":
                    finished = true;
                    this.serialPort.DataReceived -= new SerialDataReceivedEventHandler(this.OnDataRecieved);
                    break;
                case "PowerOff":
                    finished = false;
                    this.serialPort.DataReceived -= new SerialDataReceivedEventHandler(this.OnDataRecieved);
                    MessageBox.Show("Die externe Spannungsversorgung ist nicht angeschlossen!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                default:
                    DiodenWerte DiodenPunkt;

                    try
                    {
                        DiodenPunkt = new DiodenWerte(input);
                    }
                    catch
                    {
                        break;
                    }

                    bool skip = false;

                    if (ListDiodenWerte.Where(p => p.getVoltage == DiodenPunkt.getVoltage).Count() > 0)
                    {
                        skip = true;
                    }


                    if (!skip)
                    {
                        this.ListDiodenWerte.Add(DiodenPunkt);
                    }


                    break;
            }
        }

        /// <summary>
        /// Speichert die Messwerte
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "csv files (*.csv)|*.csv";
            saveFileDialog.FileName = "Kennlinie";

            if (saveFileDialog.ShowDialog() == true)
            {
                string path = saveFileDialog.FileName;
                StreamWriter sw = new StreamWriter(path);

                foreach(DiodenWerte wert in ListDiodenWerte)
                {
                    sw.WriteLine(wert.getVoltage + ";" + wert.getAmpere);
                }

                sw.Close();
                this.Status = "Die Datei wurde gespeichert.";
            }
        }

        /// <summary>
        /// Bricht die Messung ab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.serialPort.Write("Stop\n");
            this.Status = "Der Messvorgang wurde abgebrochen.";
        }
    }
}
